import React, { useState } from "react";
import "./style.css";
import HelloWorld from "./component/HelloWorld";
import HelloName from "./component/HelloName";
import PropsName from "./component/PropsName";
import Comment from "./component/Comment";
import Todo from "./component/Todo";
import WorldClock from "./component/WorldClock";
import Container from "./component/Container";

const ElementLink1 = (
  <a href="https://www.reactjs.org" target="_blank">
    test1
  </a>
);

const ElementLink2 = () => {
  return (
    <React.Fragment>
      <a href="https://www.reactjs.org" target="_blank">
        test2
      </a>
    </React.Fragment>
  );
};

// ロジックの切り出しが可能になった
const updateCount = (currentCount) => currentCount + 1;

const CountFunction = () => {
  const [count, setCount] = React.useState(0);
  const increment1 = () => {
    setCount(count + 1);
  };
  const increment2 = () => {
    setCount(updateCount);
  };
  return (
    <React.Fragment>
      <div>
        <p>count: {count}</p>
        <button onClick={increment1}>Click</button>
        <button onClick={increment2}>Click</button>
      </div>
    </React.Fragment>
  );
};

// https://www.twilio.com/blog/react-choose-functional-components-jp
class CountClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      counter: 2,
    };
  }

  // よくわからない
  // componentDidMount() {
  //   console.log("Hello class");
  // }
  // componentWillUnmount() {
  //   console.log("Bye Class");
  //   clearInterval(this.state.conter);
  // }

  render() {
    return (
      <React.Fragment>
        <div>
          <p>count: {this.state.count}</p>
          <button
            onClick={() => this.setState({ count: this.state.count + 1 })}
          >
            Click
          </button>
          <p>counter: {this.state.counter}</p>
          <button
            onClick={() => this.setState({ counter: this.state.counter + 2 })}
          >
            Click
          </button>
        </div>
      </React.Fragment>
    );
  }
}

// =====================================================
// ライフサイクルに関しての説明
const LifeCycleFunction = () => {
  React.useEffect(() => {
    console.log("Hello function");
  }, []);
  return (
    <React.Fragment>
      <h1>Hello, World Function</h1>
    </React.Fragment>
  );
};
const LifeCycleFunctionWill = () => {
  React.useEffect(() => {
    return () => {
      console.log("Bye Function");
    };
  }, []);
  return (
    <React.Fragment>
      <h1>Bye, World Function</h1>
    </React.Fragment>
  );
};

class LifeCycleClass extends React.Component {
  componentDidMount() {
    console.log("Hello class");
  }
  componentWillUnmount() {
    console.log("Bye Class");
  }
  render() {
    return (
      <React.Fragment>
        <h1>Hello, World Class</h1>
      </React.Fragment>
    );
  }
}
// class LifeCycleClassWill extends React.Component {
//   componentWillUnmount() {
//     console.log("Bye Class");
//   }
//   render() {
//     return (
//       <React.Fragment>
//         <h1>Bye, World Class</h1>
//       </React.Fragment>
//     );
//   }
// }
// =====================================================

const App = () => {
  return (
    <React.Fragment>
      <div className="orange">
        <Todo />
        <a href="#WorldClock">WorldClockにジャンプ</a>
        {/* <Element /> */}
        <label for="japan">
          <input type="checkbox" name="travel" value="日本国内" id="japan" />
          日本国内
        </label>
        <label for="europe">
          <input type="checkbox" name="travel" value="ヨーロッパ" id="europe" />
          ヨーロッパ
        </label>
        <label for="asia">
          <input type="checkbox" name="travel" value="東南アジア" id="asia" />
          東南アジア
        </label>
        <CountFunction />
        <CountClass />
        <div className="lifecyclemethod">
          <LifeCycleFunction />
          <LifeCycleFunctionWill />
          <LifeCycleClass />
        </div>
        <div id="WorldClock">
          <WorldClock />
        </div>
      </div>
      <div>
        <Container />
      </div>
      <button>test</button>
      <div className="area1">
        {/* <a href="mailto:info@example.com">お問い合わせ</a> */}
        <h1>test</h1>
        <h2>test2</h2>
        {/* <Test></Test> */}
        <HelloWorld />
        {/* <Element /> */}
        <HelloName />
        <h4>この↓の2個の昨日は同じ、</h4>
        <h4>変数として記述し、javascriptとして読み込む({})</h4>
        <h5>{ElementLink1}</h5>
        <h4>それか</h4>
        <h4>コンポーネントとして記述し読み込むかの違い</h4>
        <ElementLink2 />
        <PropsName name="Sara" id="3" />
        <PropsName name="Bob" id="4" />
        {/* <div>{Comment}</div> */}
      </div>
      <div className="area2">
        <Comment
          date={new Date()}
          text="I hope you enjoy learning React!"
          author={{
            name: "Hello Kitty",
            avatarUrl: "http://placekitten.com/g/64/64",
          }}
        />
        <Comment
          date={new Date()}
          text="React is awesome framework!"
          author={{
            name: "Hello Kitty",
            avatarUrl: "http://placekitten.com/g/64/64",
          }}
        />
      </div>
    </React.Fragment>
  );
};

export default App;
