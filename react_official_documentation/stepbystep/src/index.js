import React from "react";
import ReactDom from "react-dom";
import App from "./App";
import Tick from "./Tick";
import reportWebVitals from "./reportWebVitals";

// const root = ReactDom.createRoot(document.getElementById("root"));

// function tick() {
//   const element = (
//     <div>
//       <h1>hello world</h1>
//       <h2>It is {new Date().toLocaleDateString()}</h2>
//     </div>
//   );
//   root.render(element);
// }

// setInterval(tick, 1000);

ReactDom.render(<App />, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
