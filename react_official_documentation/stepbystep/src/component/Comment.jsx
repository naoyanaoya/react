import React from "react";

function formatDate(date) {
  return date.toLocaleTimeString();
}

const Avatar = (props) => {
  return (
    <React.Fragment>
      <img className="Avatar" src={props.user.avatarUrl} alt="avatar" />
    </React.Fragment>
  );
};

const UserInfo = (props) => {
  return (
    <React.Fragment>
      <div className="UserInfo">
        {/* props.userで止めておくと、Avatarでprops.user.avatarUrlで使えるようにできる */}
        <Avatar user={props.user} />
        <div className="UserInfo-name">{props.user.name}</div>
      </div>
    </React.Fragment>
  );
};

const Comment = (props) => {
  return (
    <React.Fragment>
      <div className="Comment">
        <UserInfo user={props.author} />

        {/* <div className="UserInfo"> */}
        {/* <img src={props.author.avatarUrl} alt={props.author.name} />
          <div className="UserInfo-name">{props.author.name}</div> */}
        {/* <Avatar user={props.author} /> */}
        {/* </div> */}
        <div className="Comment-text">{props.text}</div>
        <div className="Comment-date">{formatDate(props.date)}</div>
      </div>
    </React.Fragment>
  );
};

export default Comment;
