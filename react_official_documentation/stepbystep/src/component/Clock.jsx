import React, { Component } from "react";

class Clock extends Component {
  render() {
    return (
      <React.Fragment>
        <h2>{this.props.time}</h2>
      </React.Fragment>
    );
  }
}

export default Clock;
