import React from "react";

const PropsName = (props) => {
  return (
    <React.Fragment>
      <div>
        Test, {props.name}, {props.id}
      </div>
    </React.Fragment>
  );
};

export default PropsName;
