import React, { useState } from "react";

const Todo = () => {
  const [todoText, setTodoText] = useState("");
  const [incompleteTodos, setIncompleteTodos] = useState([
    "test1",
    "test2",
    "test3",
  ]);
  const [completeTodos, setCompleteTodos] = useState([
    "test4",
    "test5",
    "test6",
  ]);

  // これはinputに紐付くevent
  const onChangeTodoText = (event) => {
    setTodoText(event.target.value);
  };

  // これはbuttonに紐づくevent
  const onClickAdd = () => {
    if (todoText === "") {
      return;
    }
    const newTodos = [...incompleteTodos, todoText];
    setIncompleteTodos(newTodos);
    setTodoText("");
  };

  // これはbuttonに紐づくevent
  const onClickComplete = (index) => {
    const newIncompleteTodos = [...incompleteTodos];
    newIncompleteTodos.splice(index, 1);
    const newCompleteTodos = [...completeTodos, incompleteTodos[index]];
    setIncompleteTodos(newIncompleteTodos);
    setCompleteTodos(newCompleteTodos);
  };
  // これはbuttonに紐づくevent
  const onClickDelete = (index) => {
    const newIncompleteTodos = [...incompleteTodos];
    newIncompleteTodos.splice(index, 1);
    setIncompleteTodos(newIncompleteTodos);
  };
  // これはbuttonに紐づくevent
  const onClickBack = (index) => {
    const newCompleteTodos = [...completeTodos];
    newCompleteTodos.splice(index, 1);
    const newIncompleteTodos = [...incompleteTodos, completeTodos[index]];
    setIncompleteTodos(newIncompleteTodos);
    setCompleteTodos(newCompleteTodos);
  };

  return (
    <React.Fragment>
      <div className="TODO">
        <div className="input-area">
          <input
            placeholder="TODOを入力"
            value={todoText}
            onChange={onChangeTodoText}
          />
          <button onClick={onClickAdd}>add</button>
        </div>
        <div className="incomplete-area">
          <p>incomplete todo</p>
          <ul>
            {incompleteTodos.map((todo, index) => {
              return (
                <li key={todo}>
                  <div className="row">
                    <p>{todo}</p>
                    <button onClick={() => onClickComplete(index)}>
                      complete
                    </button>
                    <button onClick={() => onClickDelete(index)}>delete</button>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
        <div className="complete-area">
          <p>complete todo</p>
          <ul>
            {completeTodos.map((todo, index) => {
              return (
                <li key={todo}>
                  <div className="row">
                    <p>{todo}</p>
                    <button onClick={() => onClickBack(index)}>back</button>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Todo;
