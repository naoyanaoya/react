import React from "react";

const user1 = {
  firstName: "naoya",
  lastName: "imai",
};
const user2 = {
  firstName: "test",
  lastName: "exam",
};

function formatName(user) {
  return user.firstName + " " + user.lastName;
}

function getGreeting(user) {
  if (user) {
    return <p>Hello, {formatName(user)}!</p>;
  } else {
    return <p>Hello, Stranger!</p>;
  }
}

const HelloName = () => {
  return (
    <React.Fragment>
      <h1>{getGreeting(user1)}</h1>
      <h1>{getGreeting(user2)}</h1>
      <h1>{getGreeting()}</h1>
    </React.Fragment>
  );
};

export default HelloName;
