import React from "react";

const HelloWorld = () => {
  return (
    <React.Fragment>
      <h1>hello, world!</h1>
    </React.Fragment>
  );
};

export default HelloWorld;
