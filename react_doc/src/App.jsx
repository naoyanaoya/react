import React from "react";
import { Header } from "./components/header";
import { Main } from "./components/main";
import { Footer } from "./components/footer";

export const App = () => {
  return (
    <React.Fragment>
      <Header />
      {/* <div>test</div> */}
      <Main />
      <Footer />
    </React.Fragment>
  );
};
