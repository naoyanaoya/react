import React from "react";

export const Footer = () => {
  const style = {
    backgroundColor: "#c1ffff",
    // width: "400px",
    // height: "30px",
    padding: "8px" /*内側の余白*/,
    margin: "8px" /*外側の余白*/,
    borderRadius: "8px" /*角の丸さ*/,
  };
  return (
    <React.Fragment>
      <div style={style}>
        <h1>footer</h1>
      </div>
    </React.Fragment>
  );
};
