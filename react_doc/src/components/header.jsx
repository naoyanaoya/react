import React from "react";

export const Header = () => {
  const style = {
    backgroundColor: "#c1ffff",
    // width: "400px",
    // height: "30px",
    // padding: "8px" /*内側の余白*/,
    // margin: "8px" /*外側の余白*/,
    borderRadius: "8px" /*角の丸さ*/,
  };
  return (
    <React.Fragment>
      <div style={style}>
        <h1>header</h1>
      </div>
    </React.Fragment>
  );
};
