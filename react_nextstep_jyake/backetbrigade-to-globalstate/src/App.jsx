import { useContext } from "react";
import { Card } from "./component/molecules/Card";
import { AdminFlagContext } from "./component/providers/AdminFlagProvider";

export const App = () => {
  const { isAdmin, setIsAdmin } = useContext(AdminFlagContext);
  // const [isAdmin, setIsAdmin] = useState(false);
  // const [posts, setPosts] = useState([]);
  const onClickSwitch = () => setIsAdmin(!isAdmin);
  // 初回のrender時とisAdminが更新されたときに実行される
  // 配列[]の中身を空にすると、初回のrender時にのみ実行される
  // useEffect(() => {
  //   console.log("useEffect1が実行されました");
  // }, [isAdmin]);
  // useEffect(() => {
  //   // useEffectに空の配列[]をつけないと、無限にfetchすることになる
  //   // 空の配列[]をつけることで、初回のrender時のみ実行される
  //   // 配列に[isAdmin]をつけると、isAdminが更新されるたびに、fetchが実行される
  //   fetch("https://jsonplaceholder.typicode.com/posts", { method: "GET" })
  //     .then((response) => {
  //       if (response.ok) {
  //         return response.json();
  //       } else {
  //         return Promise.reject(new Error("error, can not fetch data"));
  //       }
  //     })
  //     .then((data) => {
  //       setPosts(data);
  //     });
  // }, []);
  // useEffect(() => {
  //   console.log("render");
  //   return () => console.log("unmounting...");
  // });

  return (
    <div>
      {isAdmin ? <span>管理者です</span> : <span>管理者以外です</span>}
      <button onClick={onClickSwitch}>切り替え</button>
      <Card />
      {/* <div>
        {posts.map((post) => (
          <div key={post.id}>{post.title}</div>
        ))}
      </div> */}
    </div>
  );
};

export default App;
