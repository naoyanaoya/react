import { EditButton } from "../atoms/EditButton";

const style = {
  width: "300px",
  height: "200px",
  margin: "8px",
  borderRadius: "8px",
  backgroundColor: "#e9dbc0",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
};

export const Card = () => {
  // このisAdminはこのファイルの中では受け渡ししているだけで
  // 一切使われていない
  // const { isAdmin } = props;

  return (
    <div style={style}>
      <p>山田太郎</p>
      <EditButton />
    </div>
  );
};
