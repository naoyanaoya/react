import React from "react";
import Router from "./router/Router";
import { RecoilRoot } from "recoil";
import "./index.css";
import { UserProvider } from "./providers/UserProvider";

function App() {
  return (
    <RecoilRoot>
      <UserProvider>
        <Router />
      </UserProvider>
    </RecoilRoot>
  );
}

export default App;
