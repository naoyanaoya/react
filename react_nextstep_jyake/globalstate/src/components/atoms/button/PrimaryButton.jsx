import React from "react";
import styled from "styled-components";
import BaseButton from "./BaseButton";

const PrimaryButton = (props) => {
  // childrenについての説明
  // 親Componentのタグの間に入れられた要素を表示する
  // <PrimaryButton>// ここの中のこと</PrimaryButton>;
  // console.log(props);
  // console.log(props.children);
  // ボタンのテキストがchildren
  const { children } = props;
  return (
    <React.Fragment>
      <SButton>{children}</SButton>
    </React.Fragment>
  );
};

// BaseButtonのcssを継承しつつ、追加のcssを書く
const SButton = styled(BaseButton)`
  background-color: #40514e;
`;

export default PrimaryButton;
