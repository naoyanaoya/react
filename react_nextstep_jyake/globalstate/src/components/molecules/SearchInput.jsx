import React from "react";
import { memo } from "react/cjs/react.production.min";
import styled from "styled-components";
import PrimaryButton from "../atoms/button/PrimaryButton";
import Input from "../atoms/input/input";

const SearchInput = memo(() => {
  console.log("SearchInput");
  return (
    <SContainer>
      <Input placeholder="検索条件を入力" />
      <SButtonWrapper>
        <PrimaryButton>検索</PrimaryButton>
      </SButtonWrapper>
    </SContainer>
  );
});

const SContainer = styled.div`
  display: flex;
  align-items: center;
`;

const SButtonWrapper = styled.div`
  padding: 20px 8px;
`;

export default SearchInput;
