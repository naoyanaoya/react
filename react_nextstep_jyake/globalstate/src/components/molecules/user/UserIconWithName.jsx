import React from "react";
import { memo } from "react/cjs/react.production.min";
import { useRecoilValue } from "recoil";
import styled from "styled-components";
// import { UserContext } from "../../../providers/UserProvider";
import { userState } from "../../../store/userState";

const UserIconWithName = memo((props) => {
  console.log("UserIconWithName");
  const { image, name } = props;
  // const { userInfo } = useContext(UserContext);
  const userInfo = useRecoilValue(userState);
  // console.log(context);
  const isAdmin = userInfo ? userInfo.isAdmin : false;
  // console.log(props);

  return (
    <React.Fragment>
      <SContainer>
        <SImg height={160} width={160} src={image} alt={name} />
        <SName>{name}</SName>
        {isAdmin && <SEdit>編集</SEdit>}
      </SContainer>
    </React.Fragment>
  );
});

const SContainer = styled.div`
  text-align: center;
`;
const SImg = styled.img`
  border-radius: 50%;
`;
const SName = styled.p`
  font-size: 18px;
  font-weight: bold;
  color: #40514e;
`;
const SEdit = styled.span`
  text-decoration: underline;
  color: #aaa;
  cursor: pointer;
`;

export default UserIconWithName;
