import { useState } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const onClickUsers = () => {
    // alert("users");
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onClickUser1 = () => {
    // alert("user1");
    axios
      .get("https://jsonplaceholder.typicode.com/users/1")
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // 外部APIから取得したデータ
  const [items, setItems] = useState([]);
  const onClickAlgolia = () => {
    axios
      .get("https://hn.algolia.com/api/v1/search?query=react")
      .then((res) => {
        console.log(res.data.hits);
        setItems(res.data.hits);
      });
  };

  return (
    <div className="App">
      <button onClick={onClickUsers}>users</button>
      <br />
      <button onClick={onClickUser1}>id=1のuser</button>
      <br />
      <button onClick={onClickAlgolia}>algolia/api/v1/search</button>
      <br />
      <br />
      <ul>
        {items.map((item) => (
          <>
            <li key={item.objectID}>
              <p>{item.author}</p>
              <a href={item.url}>${item.title}</a>
            </li>
            <br />
          </>
        ))}
      </ul>
    </div>
  );
}

export default App;
