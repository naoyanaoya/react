import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";

import Router from "./router/Router";

import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <React.Fragment>
        <div className="App">
          <Link to="/">Home</Link>
          <br />
          <Link to="/page1">Page1</Link>
          <br />
          <Link to="/page2">Page2</Link>
        </div>
        <Router />
      </React.Fragment>
    </BrowserRouter>
  );
}

export default App;
