import React from "react";
import { useLocation, useHistory } from "react-router-dom";

const Page1DetailA = () => {
  const { state } = useLocation();
  console.log(state);
  const history = useHistory();
  const onClickBack = () => {
    history.goBack();
  };
  return (
    <React.Fragment>
      <div>
        <h1>Page1DetailAページです</h1>
        {/* {state.map((index) => {
          return (
            <React.Fragment key={state.id}>
              {console.log(state[0])}
              {console.log(index)}
            </React.Fragment>
          );
        })} */}
      </div>
      <button
        onClick={onClickBack}
        style={{ "background-color": "aqua", margin: "10px 0px" }}
      >
        戻る
      </button>
    </React.Fragment>
  );
};

export default Page1DetailA;
