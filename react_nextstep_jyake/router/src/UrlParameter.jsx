import React from "react";
import { useParams, useLocation } from "react-router-dom";

const UrlParameter = () => {
  const { id } = useParams();
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  return (
    <React.Fragment>
      <div>
        <h1>UrlParameterです</h1>
        <p>パラメーターは{id}です</p>
        <p>クエリパラメータは{query.get("name")}</p>
      </div>
    </React.Fragment>
  );
};

export default UrlParameter;
