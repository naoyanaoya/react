import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "../Home";
// import Page1 from "../Page1";
// import Page1DetailA from "../Page1DetailA";
// import Page1DetailB from "../Page1DetailB";
// import Page2 from "../Page2";
import page1Routes from "./Page1Routes";
import page2Routes from "./Page2Routes";
import Page404 from "../Page404";

const Router = () => {
  return (
    <React.Fragment>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route
          path="/page1"
          render={({ match: { url } }) => (
            <Switch>
              {/* {console.log(url)} */}
              {page1Routes.map((route) => (
                <Route
                  key={route.path}
                  exact={route.exact}
                  path={`${url}${route.path}`}
                >
                  {route.children}
                </Route>
              ))}
            </Switch>
          )}
        />
        <Route
          path="/page2"
          render={({ match: { url } }) => (
            <Switch>
              {/* {console.log(url)} */}
              {page2Routes.map((route) => (
                <Route
                  key={route.path}
                  exact={route.exact}
                  path={`${url}${route.path}`}
                >
                  {console.log(route)}
                  {route.children}
                </Route>
              ))}
            </Switch>
          )}
        />
        <Route>
          <Page404 path="*" />
        </Route>
      </Switch>
    </React.Fragment>
  );
};

export default Router;
