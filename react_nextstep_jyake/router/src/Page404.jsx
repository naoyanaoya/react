import React from "react";
import { Link } from "react-router-dom";

const Page404 = () => {
  return (
    <React.Fragment>
      <div>
        <h1>ページがみつかりません</h1>
        <Link to="/">トップに戻る</Link>
      </div>
    </React.Fragment>
  );
};

export default Page404;
