import React from "react";
import { Link, useHistory } from "react-router-dom";

const Page1 = () => {
  const arr = [...Array(100).keys()];
  console.log(arr);
  const history = useHistory();
  console.log(history);
  const onclickDetailA = () => {
    history.push("/page1/detailA");
  };
  return (
    <React.Fragment>
      <div>
        <h1>Page1ページです</h1>
        <Link to={{ pathname: "/page1/detailA", state: arr }}>DetailA</Link>
        <br />
        <Link to="/page1/detailB">DetailB</Link>
        <br />
        <button
          onClick={onclickDetailA}
          style={{ "background-color": "aqua", margin: "10px 0px" }}
        >
          DetailA
        </button>
      </div>
    </React.Fragment>
  );
};

export default Page1;
