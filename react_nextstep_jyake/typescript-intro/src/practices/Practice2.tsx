export const Practice2 = () => {
  const getTotalFee = (price: number): number => {
    const totalPrice: number = price * 1.1;
    return totalPrice;
  };
  const onClickPractice = () => {
    console.log(getTotalFee(1000));
  };
  return (
    <>
      <div>
        <p>練習問題:返却値の型指定</p>
        <button onClick={onClickPractice}>練習問題2を実行</button>
      </div>
    </>
  );
};
