import React from "react";
import Router from "./router/Router";
import "./index.css";

function App() {
  return <Router />;
}

export default App;
