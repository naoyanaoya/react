import React from "react";
import styled from "styled-components";

const Input = (props) => {
  // console.log(props);
  // console.log(props.placeholder);
  const { placeholder } = props;
  return (
    <>
      <SInput type="text" placeholder={placeholder}></SInput>
    </>
  );
};

const SInput = styled.input`
  background-color: #ffffff;
  padding: 8px 17px;
  border: 1px solid #ddd;
  border-radius: 999px;
  outline: none;
`;

export default Input;
