import styled from "styled-components";

const SecondaryButton = (props) => {
  // childrenについての説明
  // 親Componentのタグの間に入れられた要素を表示する
  // <PrimaryButton>// ここの中のこと</PrimaryButton>;
  // console.log(props);
  // console.log(props.children);
  // ボタンのテキストがchildren
  const { children } = props;
  return (
    <>
      <SButton>{children}</SButton>
    </>
  );
};

const SButton = styled.button`
  background-color: #11999e;
  color: #fff;
  padding: 6px 24px;
  border: none;
  border-radius: 999px;
  outline: none;
  &:hover {
    cursor: pointer;
    opacity: 0.8;
  }
`;

export default SecondaryButton;
