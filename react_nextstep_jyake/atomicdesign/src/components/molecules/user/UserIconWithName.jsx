import React from "react";
import styled from "styled-components";

const UserIconWithName = (props) => {
  const { image, name } = props;
  // console.log(props);
  return (
    <React.Fragment>
      <SContainer>
        <SImg height={160} width={160} src={image} alt={name} />
        <SName>{name}</SName>
      </SContainer>
    </React.Fragment>
  );
};

const SContainer = styled.div`
  text-align: center;
`;
const SImg = styled.img`
  border-radius: 50%;
`;
const SName = styled.p`
  font-size: 18px;
  font-weight: bold;
  color: #40514e;
`;

export default UserIconWithName;
