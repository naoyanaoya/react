import React from "react";
import styled from "styled-components";
import Card from "../../atoms/card/Card";
import UserIconWithName from "../../molecules/user/UserIconWithName";

const UserCard = (props) => {
  // console.log(props);
  const { user } = props;
  return (
    <React.Fragment>
      <Card>
        {/* <img height={160} width={160} src={user.image} alt={user.name} />
        <p>{user.name}</p> */}
        <UserIconWithName image={user.image} name={user.name} />
        <SDl>
          <dt>メール</dt>
          <dd>{user.email}</dd>
          <dt>電話番号</dt>
          <dd>{user.phone}</dd>
          <dt>会社名</dt>
          <dd>{user.company.name}</dd>
          <dt>webサイト</dt>
          <dd>{user.website}</dd>
        </SDl>
      </Card>
    </React.Fragment>
  );
};

const SDl = styled.dl`
  text-align: left;
  dt {
    float: left;
  }
  dd {
    padding-left: 100px;
    padding-bottom: 8px;
    overflow-wrap: break-word;
  }
`;

export default UserCard;
