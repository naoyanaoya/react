import axios from "axios";
import { useState } from "react";
import "./App.css";
import { UserCard } from "./components/UserCard";
import { User } from "./types/api/user";
import { UserProfile } from "./types/userProfile";
import { useAllUsers } from "./hooks/useAllUsers";

const user = {
  id: 1,
  name: "なおや",
  email: "email.com",
  address: "Address",
};

function App() {
  const { getUsers, UserProfiles, loading, error } = useAllUsers();
  const onClickFetchUser = () => {
    getUsers();
  };
  return (
    <div className="App">
      <button onClick={onClickFetchUser}>データ取得</button>
      <br />
      {error ? (
        <p style={{ color: "red" }}>データの取得に失敗しました</p>
      ) : loading ? (
        <p>Loading...</p>
      ) : (
        <>
          {UserProfiles.map((user) => (
            <UserCard key={user.id} user={user} />
          ))}
        </>
      )}
      <UserCard user={user} />
    </div>
  );
}

export default App;
