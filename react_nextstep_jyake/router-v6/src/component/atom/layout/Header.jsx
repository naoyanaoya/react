import { Link } from "react-router-dom";
import styled from "styled-components";

export const Header = () => {
  return (
    <SHeader>
      <SLink to="/">TOPPAGE</SLink>
      <SLink to="/about">ABOUT</SLink>
      <SLink to="/Products">PRODUCTS</SLink>
    </SHeader>
  );
};

const SHeader = styled.header`
  background-color: #11999e;
  color: #fff;
  text-align: center;
  padding: 8px 0;
`;
const SLink = styled(Link)`
  margin: 0 8px;
`;
