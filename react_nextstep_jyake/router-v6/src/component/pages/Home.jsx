import { Link } from "react-router-dom";

export const Home = () => {
  return (
    <>
      <div>
        <p>これはトップページです</p>
        <ul>
          <li>
            <Link to="/">TOPPAGE</Link>
          </li>
          <li>
            <Link to="/about">ABOUT</Link>
          </li>
          <li>
            <Link to="/Products">PRODUCTS</Link>
          </li>
        </ul>
      </div>
    </>
  );
};
