import { useParams } from "react-router-dom";
import { getInvoice } from "../data";

export const Invoice = () => {
  let params = useParams();
  let invoice = getInvoice(parseInt(params.invoiceId, 10));
  return (
    <>
      {/* {console.log(params.InvoiceId)} */}
      {/* {console.log(params)}
      {console.log(invoice)} */}
      <main style={{ padding: "1rem" }}>
        <h2>Total Due: {invoice.amount}</h2>
        <p>
          {invoice.name}: {invoice.number}
        </p>
        <p>Due Date: {invoice.due}</p>
      </main>
    </>
  );
};
