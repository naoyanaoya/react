import { Outlet, Link, NavLink } from "react-router-dom";
import "./App.css";

export const App = () => {
  return (
    <div>
      <h1>Bookkeeper!</h1>
      <nav style={{ borderBottom: "solid 1px", paddingBottom: "1rem" }}>
        <NavLink
          // style={({ isActive }) => (isActive ? { color: "blue" } : undefined)}
          className={({ isActive }) => (isActive ? "active" : undefined)}
          to="/"
        >
          Home
        </NavLink>
        |{" "}
        <NavLink
          className={({ isActive }) => (isActive ? "active" : undefined)}
          to="/invoices"
        >
          Invoices
        </NavLink>
        |{" "}
        <NavLink
          className={({ isActive }) => (isActive ? "active" : undefined)}
          to="/expenses"
        >
          Expenses
        </NavLink>
      </nav>
      <Outlet />
    </div>
  );
};
