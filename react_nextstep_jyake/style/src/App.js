import React from "react";
import "./App.css";
import InlineStyle from "./components/InlineStyle";
import CssModules from "./components/CssModules";
import StyledJsx from "./components/StyledJsx";
import StyledComponents from "./components/StyledComponents";
import Emotion from "./components/Emotion";

function App() {
  return (
    <React.Fragment>
      <InlineStyle />
      <CssModules />
      <StyledJsx />
      <StyledComponents />
      <Emotion />
    </React.Fragment>
  );
}

export default App;
