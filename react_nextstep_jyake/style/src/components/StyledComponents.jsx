import React from "react";
import styled from "styled-components";

const StyledComponents = () => {
  return (
    <React.Fragment>
      <SContainer>
        <STitle>- styled components -</STitle>
        <SButton>test</SButton>
      </SContainer>
    </React.Fragment>
  );
};

const SContainer = styled.div`
  border: solid 2px #392eff;
  border-radius: 20px;
  padding: 8px;
  margin: 8px;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;
const STitle = styled.p`
  color: #3d84a8;
`;
const SButton = styled.p`
  background-color: #abedd8;
  border: none;
  padding: 8px;
  border-radius: 8px;
  &:hover {
    background-color: #47cdcf;
    color: #fff;
    cursor: pointer;
  }
`;

export default StyledComponents;
