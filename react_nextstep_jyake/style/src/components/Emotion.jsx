/** @jsxRuntime classic */
/** @jsx jsx */
import React from "react";
import { jsx, css } from "@emotion/react";
import styled from "@emotion/styled";

const Emotion = () => {
  const containerStyle = css`
    border: solid 2px #392eff;
    border-radius: 20px;
    padding: 8px;
    margin: 8px;
    display: flex;
    justify-content: space-around;
    align-items: center;
  `;
  const titleStyle = css({
    color: "#3d84a8",
  });
  return (
    <React.Fragment>
      <div css={containerStyle}>
        <p css={titleStyle}>- CssModules -</p>
        <SButton>test</SButton>
      </div>
    </React.Fragment>
  );
};

const SButton = styled.button`
  background-color: #abedd8;
  border: none;
  padding: 8px;
  border-radius: 8px;
  &:hover {
    background-color: #47cdcf;
    color: #fff;
    cursor: pointer;
  }
`;

export default Emotion;
