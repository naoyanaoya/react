import React from "react";
import classes from "./CssModules.module.scss";

const CssModules = () => {
  return (
    <React.Fragment>
      <div className={classes.container}>
        <p className={classes.title}>- CssModules -</p>
        <button className={classes.button}>test</button>
      </div>
    </React.Fragment>
  );
};

export default CssModules;
