import React, { useState, useCallback } from "react";
import logo from "./logo.svg";
import "./App.css";
import ChildArea from "./ChildArea";

function App() {
  console.log("App");
  const [count, setCount] = useState(0);
  const onClickCountUp = () => {
    setCount(count + 1);
  };
  const [text, setText] = useState("");
  const onChangeText = (e) => {
    setText(e.target.value);
  };
  const [open, setOpen] = useState(false);
  const onClickOpen = () => {
    setOpen(!open);
  };
  const onClickClose = useCallback(() => {
    setOpen(false);
  }, []);

  return (
    <React.Fragment>
      <div className="App">
        {/* <h1>hello</h1>
        <p>{count}</p>
        <button onClick={onClickCountUp}>カウントアップ</button> */}
        <input onChange={onChangeText} />
        <br />
        <br />
        <button onClick={onClickOpen}>表示</button>
        <ChildArea open={open} onClickClose={onClickClose} />
      </div>
    </React.Fragment>
  );
}

export default App;
