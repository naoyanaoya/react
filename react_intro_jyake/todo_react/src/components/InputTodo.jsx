import React from "react";

export const InputTodo = (props) => {
  const { todoText, onChange, onClick, disabled } = props;
  const style = {
    backgroundColor: "#c1ffff",
    width: "400px",
    height: "30px",
    padding: "8px" /*内側の余白*/,
    margin: "8px" /*外側の余白*/,
    borderRadius: "8px" /*角の丸さ*/,
  };
  return (
    <React.Fragment>
      <div style={style} className="input-area">
        <input
          disabled={disabled}
          placeholder="TODOを入力"
          value={todoText}
          onChange={onChange}
        />
        <button disabled={disabled} onClick={onClick}>
          追加
        </button>
      </div>
    </React.Fragment>
  );
};
