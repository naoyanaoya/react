// import "./style.css";

document.getElementById("add-button").addEventListener("click", () => {
  onClickAdd();
});

const onClickAdd = () => {
  // Console.log("add-text");
  // テキストボックスの値を取得し初期化する
  const $addText = document.getElementById("add-text");
  const inputText = $addText.value;
  // var inputText_var = document.getElementById("add-text").value;
  // console.log(inputText);
  // console.log(inputText_var);
  if (inputText === "" || inputText === null) {
    // alert("null です");
    return;
  }
  const $priority = document.getElementsByName("priority");
  // console.log(priority);
  const priorityLen = $priority.length;
  // console.log(priorityLen);
  // console.checkValue = "";
  for (let i = 0; i < priorityLen; i++) {
    if ($priority.item(i).checked) {
      checkValue = $priority.item(i).value;
    }
  }
  // console.log("checked is " + checkValue + " .");
  // console.log(checkValue);
  // alert(inputText);

  // inputTExt= ""にすると関数にわたすときに""になるので、リストに名前無しが追加される。
  // inputText = "";
  document.getElementById("add-text").value = "";

  addIncompleteList(inputText, checkValue);
};

// 未完了リストに追加する関数
const addIncompleteList = (text, priority) => {
  // console.log(priority);
  const $completeButton = document.createElement("button");
  $completeButton.innerText = "complete";
  $completeButton.addEventListener("click", () => {
    // 押された削除ボタンの親タグ(li)を未完了リストから削除
    // const $deleteTarget = $deleteButton.parentNode.parentNode;
    // console.log($deleteTarget);
    const $deleteTarget_ = $deleteButton.closest("li");
    // console.log($deleteTarget_);
    deleteFromIncompleteList($deleteTarget_);

    // 完了リストに追加する要素
    const $addTarget = $completeButton.closest("li");
    console.log($addTarget);

    // TODO内容テキストを取得
    const text = $addTarget.firstElementChild.firstElementChild.innerText;
    console.log($addTarget);
    console.log(text);

    // div以下を初期化;
    // $addTarget.textContent = null;

    // div生成
    const $div = document.createElement("div");
    $div.className = "list-row";
    // li生成
    const $li = document.createElement("li");
    // pタグの生成
    const $p1 = document.createElement("p");
    $p1.innerText = text;
    const $p2 = document.createElement("p");
    $p2.innerText = priority;
    $p2.classList.add("none");

    // back buttonの生成
    const $backButton = document.createElement("button");
    $backButton.innerText = "back";
    $backButton.addEventListener("click", () => {
      // 押された戻すボタンの親タグを完了リストから削除
      // const $deleteTarget = $backButton.parentNode.parentNode;
      const $deleteTarget_ = $backButton.closest("li");
      // console.log($deleteTarget);
      console.log($deleteTarget_);
      document.getElementById("complete-list").removeChild($deleteTarget_);

      // テキストの取得
      const text = $backButton.closest("div").firstElementChild.innerText;
      const priority =
        $backButton.parentNode.getElementsByClassName("none")[0].innerText;
      // console.log(backButton.parentNode);
      // console.log(priority);
      // console.log(backButton.parentNode);
      // const priority = backButton.parentNode.firstElementChild.innerText;
      addIncompleteList(text, priority);
    });

    $div.appendChild($p1);
    $div.appendChild($p2);
    $div.appendChild($backButton);
    $li.appendChild($div);

    // 完了したTODOに追加
    document.getElementById("complete-list").appendChild($li);
  });

  const $deleteButton = document.createElement("button");
  $deleteButton.innerText = "delete";
  // console.log(deleteButton);
  $deleteButton.addEventListener("click", () => {
    // alert("削除");
    // 押された削除ボタンの親タグ(li)を未完了リストから削除
    const $deleteTarget = $deleteButton.parentNode.parentNode;
    // console.log(deleteTarget);
    // document.getElementById("incomplete-list").removeChild(deleteTarget);

    deleteFromIncompleteList($deleteTarget);
  });

  // li作成
  const $li = document.createElement("li");
  // console.log(li);

  // div生成
  const $div = document.createElement("div");
  $div.className = "list-row";
  // console.log(div);

  // p作成
  const $p1 = document.createElement("p");
  $p1.innerText = text;
  const $p2 = document.createElement("p");
  $p2.innerText = priority;
  if (priority === "high") {
    $p2.classList.add("red");
  } else if (priority === "low") {
    $p2.classList.add("blue");
  }
  // console.log(p);

  // divタグの子要素にpを設定
  $div.appendChild($p1);
  $div.appendChild($p2);
  $div.appendChild($completeButton);
  $div.appendChild($deleteButton);
  // liタグの子要素に各要素を設定
  $li.appendChild($div);
  // console.log(li);
  // 未完了ulに追加
  document.getElementById("incomplete-list").appendChild($li);
};

// 未完了リストから指定の要素を削除
const deleteFromIncompleteList = ($target) => {
  // console.log(deleteTarget);
  document.getElementById("incomplete-list").removeChild($target);
};

const CompleteList = document.querySelectorAll(`[list-name="complete"]`);
const CompleteListArray = document.querySelectorAll(
  `[list-name="complete"]`
)[0];
// console.log(CompleteList);
// console.log(CompleteListArray);
const CompleteListArrayStyleCursor = document.querySelectorAll(
  `[list-name="complete"]`
)[0].style.cursor;
// console.log(CompleteListArrayStyleCursor);
CompleteListArray.style.cursor = "pointer";

CompleteListArray.addEventListener("click", (e) => handleClick(e));
handleClick = (e) => {
  const $this = e.target;
  const $thisStyle = $this.style;
  // console.log($this);
  // console.log($thisStyle);
};

// CompleteListArray.classList.add("pointer");
// const CompleteListArray.addEventListener("click", (e) => handleClick(e));
// const handleClick = (e) => {
//   const $this = e.target;
//   console.log($this);
// };
// .getElementById("complete-list")
// .addEventListener("click", (e) => handleClick(e));

// const handleClick = (e) => {
//   const $this = e.target;
//   console.log($this);
// };
