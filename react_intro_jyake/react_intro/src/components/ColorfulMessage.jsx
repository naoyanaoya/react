import React from "react";

const ColorfulMessage = (props) => {
  // console.log("test");
  const { color, children } = props;
  // console.log(props);
  const contentStyle = {
    color: color,
    fontSize: "18px",
  };
  return (
    <React.Fragment>
      <p style={contentStyle}>{children}</p>
    </React.Fragment>
  );
};

export default ColorfulMessage;
